package ro.ase.eu.sm.seriesmanager.model.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity(name="analysis")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class Analysis {

    @Id
    @Column(name="show_id")
    private Long show_id;

    @Column(name="text_length_avg")
    private Double textLengthAvg;

    @Column(name="html_length_avg")
    private Double htmlLengthAvg;

    @Column(name="out_links_avg")
    private Double outLinksAvg;
//string sentiment
    @Column(name="sentiment_tones")
    private String sentimentTones;

    @Column(name="sentiment_score")
    private Integer sentimentScore;

    @Column(name="searches_level")
    private Double searchesLevel;

    @Column(name="imdb_rating")
    private Double imdbRating;

    @Column(name="imdb_votes")
    private Integer imdbVotes;

    @Column(name="external_rating")
    private Double externalRating;

    @OneToOne
    Show show;

    @Override
    public String toString() {
        return "";
    }
}
