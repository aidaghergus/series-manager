package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.*;
import ro.ase.eu.sm.seriesmanager.repository.ShowRepository;
import ro.ase.eu.sm.seriesmanager.service.ShowService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShowServiceImpl implements ShowService{

    @Autowired
    ShowRepository showRepository;

    @Override
    public List<Show> findAll() {
        return showRepository.findAll();
    }

    @Override
    public Show findShowById(Long showId) {
        return showRepository.findOne(showId);
    }

    @Override
    public List<Category> findShowCategories(Long showID) {

        Show show=showRepository.getOne(showID);
        return show.getCategorieList();
    }

    @Override
    public Double getShowStars(Long showId) {
        Show show=showRepository.getOne(showId);
        List<Integer> stars=show.getReviews().stream().map(review -> review.getStars()).collect(Collectors.toList());
        Double average = stars.stream().mapToInt(val -> val).average().orElse(0.0);
        return average;
    }

    @Override
    public List<Episode> findShowEpisodes(Long showId) {
        Show show=showRepository.getOne(showId);
        return show.getEpisodeList();
    }

    @Override
    public List<Review> findShowReviews(Long showId) {
        Show show=showRepository.getOne(showId);
        return show.getReviews();
    }

    @Override
    public List<Comment> findShowComments(Long showId) {
        Show show=showRepository.getOne(showId);
        return show.getComments();
    }

    @Override
    public List<Episode> findEpisodesBySeason(Long showId, String season) {
        Show show=showRepository.getOne(showId);
        return show.getEpisodeList().stream().filter(episode -> season.equals(episode.getSeason())).collect(Collectors.toList());
    }

    @Override
    public Show findShowInDatabase(String filter) {
        List<Show> list=showRepository.findByNameContaining(filter);
        if(list.size()==0)
            return null;
        return list.get(0);

    }

    @Override
    public void save(Show show) {
        showRepository.save(show);
    }

    @Override
    public List<Show> findTopShows() {
        return showRepository.findTop6ByOrderByImdbRatingDesc();
    }
}













