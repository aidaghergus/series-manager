package ro.ase.eu.sm.seriesmanager.errorhandling.exception.general;

public class InvalidFieldsException  extends RuntimeException {

    public InvalidFieldsException(final String error) {
        super(error);
    }
}
