package ro.ase.eu.sm.seriesmanager.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity(name="reviews")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Review {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="review_id")
    private Long id;

    @Column(name="stars", nullable=false)
    private Integer stars;

    @Column(name="date", columnDefinition="TIMESTAMP DEFAULT NOW()")
    private Date reviewDate;

    @Column(name="comment")
    private String comment;

    @ManyToOne
    @JoinColumn(name="user_id")
    @NotNull
    private User user;

    @ManyToOne
    @JoinColumn(name="show_id")
    @NotNull
    private Show show;
}
