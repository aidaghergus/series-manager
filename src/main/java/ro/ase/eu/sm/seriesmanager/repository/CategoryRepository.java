package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ase.eu.sm.seriesmanager.model.entity.Category;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, String> {

}
