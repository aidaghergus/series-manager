package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class InvalidUsernameException extends RuntimeException {

    public InvalidUsernameException(final String error) {
        super(error);
    }
}
