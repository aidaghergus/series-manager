package ro.ase.eu.sm.seriesmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.RoleMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.RoleDTO;
import ro.ase.eu.sm.seriesmanager.service.RoleService;


import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RoleController {

    private static final Logger LOGGER = new Logger(RoleController.class);

    @Autowired
    RoleService roleService;

    @GetMapping("/roles")
    public List<RoleDTO> getAllRoles() {
        LOGGER.info(InfoMessage.REQUEST_RECEIVE + RequestType.GET_ROLES);
        return roleService.findAll().stream().map(RoleMapper::mapToDTO).collect(Collectors.toList());
    }




}
