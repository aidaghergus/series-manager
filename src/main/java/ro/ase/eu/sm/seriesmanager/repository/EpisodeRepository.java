package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ase.eu.sm.seriesmanager.model.entity.Episode;

import java.util.List;

public interface EpisodeRepository extends JpaRepository<Episode, Long> {

    List<Episode> findAllByShowId(Long id);

    List<Episode> findAllByShowIdAndAndSeason(Long showId, String season);
}
