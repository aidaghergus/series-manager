package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.CategoryDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Category;

import java.util.stream.Collectors;

public class CategoryMapper {

    private static final Logger LOGGER = new Logger(CommentMapper.class);

    public CategoryMapper(){}

    public static Category mapToEntity(CategoryDTO categoryDTO, RepositoryMapper repositoryMapper){
        Category category=new Category(categoryDTO.getName(), null);
        return category;
    }

    public static CategoryDTO mapToDTO(Category category){
        return CategoryDTO.builder()
                .name(category.getName())
                .build();
    }

}
