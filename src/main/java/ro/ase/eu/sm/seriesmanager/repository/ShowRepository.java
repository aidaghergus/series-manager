package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;

import java.util.List;

public interface ShowRepository extends JpaRepository<Show, Long> {

    List<Show> findByNameContaining(String name);

    //@Query("select s from Show s where LOWER(s.name ) LIKE CONCAT('%',LOWER(:filter),'%')")
    //List<Show> findFilteredShow(@Param("filter") String filter);


    @Query(value="select sum(search_level) from sm.shows", nativeQuery = true)
    Integer searches();

    List<Show> findTop6ByOrderByImdbRatingDesc();



}
