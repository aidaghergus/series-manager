package ro.ase.eu.sm.seriesmanager.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.List;

@Entity(name="shows")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Show {

    @Id
    @Column(name="show_id")
    private Long id;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="overview", nullable=false)
    private String description;

    @Column(name="year", nullable=false)
    private Integer year;

    @Column(name="image", nullable=false)
    private String image;

    @Column(name="imdb_rating")
    private Double imdbRating;

    @Column(name="imdb_votes")
    private Integer imdbVotes;

    @Column(name="external_rating")
    private Double externalRating;

    @Column(name="actors")
    private String actors;

    @Column(name="awards")
    private String awards;

    @Column(name="search_level")
    private Integer searches;

    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "show")
    private List<Episode> episodeList;


    @NotFound(
            action = NotFoundAction.IGNORE)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinTable(
            name = "category_show",
            joinColumns = @JoinColumn(name = "show_id", referencedColumnName = "show_id"),
            inverseJoinColumns = @JoinColumn(name = "category_name", referencedColumnName = "category_name"))
    private List<Category> categorieList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "show")
    private List<Review> reviews;

    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "show")
    private List<Comment> comments;

    @Override
    public String toString() {
        return "";
    }
}
