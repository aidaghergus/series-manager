package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class CredentialsRequiredException extends RuntimeException {

    public CredentialsRequiredException(final String error) {
        super(error);
    }
}
