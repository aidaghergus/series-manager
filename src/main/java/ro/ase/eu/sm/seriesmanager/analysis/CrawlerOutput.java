package ro.ase.eu.sm.seriesmanager.analysis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CrawlerOutput {
    private double textLength;
    private double htmlLength;
    private double outLinks;

    public static CrawlerOutput createAverage(List<CrawlerOutput> list){
        double textLengthAvg=0;
        double htmlLengthAvg=0;
        double outLinksAvg=0;
        for(CrawlerOutput c:list){
            textLengthAvg+=c.getTextLength();
            htmlLengthAvg+=c.getHtmlLength();
            outLinksAvg+=c.getOutLinks();
        }
        return new CrawlerOutput(textLengthAvg/list.size(),htmlLengthAvg/list.size(),outLinksAvg/list.size());
    }
}
