package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.AnalysisDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;

public class AnalysisMapper {

    private static final Logger LOGGER = new Logger(AnalysisMapper.class);

    public static Analysis mapToEntity(AnalysisDTO analysisDTO, RepositoryMapper mapper){
        Show show=mapper.getShowRepository().findOne(analysisDTO.getShow_id());
        Analysis analysis=new Analysis(analysisDTO.getShow_id(),analysisDTO.getTextLengthAvg(),analysisDTO.getHtmlLengthAvg(),analysisDTO.getOutLinksAvg(),analysisDTO.getSentimentTones(),analysisDTO.getSentimentScore(),analysisDTO.getSearchesLevel(),analysisDTO.getImdbRating(),analysisDTO.getImdbVotes(),analysisDTO.getExternalRating(),show);
        return  analysis;
    }

    public static AnalysisDTO mapToDTO(Analysis analysis){
        return AnalysisDTO.builder().show_id(analysis.getShow_id())
                .show_name(analysis.getShow().getName())
                .textLengthAvg(analysis.getTextLengthAvg())
                .htmlLengthAvg(analysis.getHtmlLengthAvg())
                .outLinksAvg(analysis.getOutLinksAvg())
                .searchesLevel(analysis.getSearchesLevel())
                .sentimentScore(analysis.getSentimentScore())
                .imdbRating(analysis.getImdbRating())
                .imdbVotes(analysis.getImdbVotes())
                .externalRating(analysis.getExternalRating())
                .sentimentTones(analysis.getSentimentTones())
                .build();
    }

}
