package ro.ase.eu.sm.seriesmanager.http.response;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private String message;

    public ErrorResponse() {
        super();
    }


    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
