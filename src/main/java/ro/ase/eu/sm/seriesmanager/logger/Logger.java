package ro.ase.eu.sm.seriesmanager.logger;

import org.slf4j.LoggerFactory;

public class Logger {

	private org.slf4j.Logger log;

	@SuppressWarnings("rawtypes")
	public Logger(final Class aClassName) {
		log = LoggerFactory.getLogger(aClassName);
	}

	public void info(final String message, Throwable e) {
		log.info(message, e);
	}

	public void info(final Object object, final String message) {
		if(object!=null) {
			log.info(message + " - " + object.toString());
		}
		else {
			info(message);
		}
	}

	public void info(final String message) {
		log.info(message);
	}

	public void error(final String message, Throwable e) {
		log.error(message,e);
	}

	public void error(final Object object, final String message) {
		log.error(message + " - " + object.toString());
	}

	public void error(final String message) {
		log.error(message);
	}

}
