package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ase.eu.sm.seriesmanager.model.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    public Role findByRoleName(String roleName);


}
