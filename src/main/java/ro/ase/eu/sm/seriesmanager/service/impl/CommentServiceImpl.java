package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.Comment;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.repository.CommentRepository;
import ro.ase.eu.sm.seriesmanager.service.CommentService;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Override
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> findByUser(User user) {
        return commentRepository.findAllByUserId(user.getId());
    }

    @Override
    public List<Comment> findByShow(Long showId) {
        return commentRepository.findAllByShowId(showId);
    }
}
