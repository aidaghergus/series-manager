package ro.ase.eu.sm.seriesmanager.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity(name="categories")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Category {

    @Id
    @Column(name="category_name", nullable=false)
    private String name;

    @ManyToMany(mappedBy = "categorieList")
    private List<Show> showsList;
}
