package ro.ase.eu.sm.seriesmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ro.ase.eu.sm.seriesmanager.errorhandling.error.ErrorMessage;
import ro.ase.eu.sm.seriesmanager.errorhandling.exception.user.NotAuthorizedException;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.TokenService;

import java.util.List;

@Component
@PropertySource("classpath:sm.properties")
public class Authenticator {

    private static final Logger LOGGER = new Logger(Authenticator.class);

    @Value("${users.exiredDays}")
    private int expiredDays;

    @Autowired
    private TokenService tokenService;

    public Token obtainToken(User user) {
        List<Token> tokens = tokenService.findByUser(user);
        tokens.forEach(t->tokenService.delete(t));
        Token newToken = new Token(expiredDays);
        newToken.setUser(user);
        tokenService.save(newToken);
        LOGGER.info(user, InfoMessage.TOKEN_OBTAINED + newToken.getToken());
        return newToken;
    }

    public Long authenticateWithToken(String token) throws NotAuthorizedException {
        Long userId = null;
        Token existingToken = tokenService.findByToken(token);
        if(existingToken==null) {
            LOGGER.error(ErrorMessage.INVALID_TOKEN + token);
            throw new NotAuthorizedException(ErrorMessage.NOT_AUTHENTICATED);
        }
        else if(existingToken.isExpired()) {
            throw new NotAuthorizedException(ErrorMessage.SESSION_EXPIRED);
        }
        LOGGER.info(InfoMessage.SUCCESFULLY_AUTH_TOKEN);
        return existingToken.getUser().getId();
    }

}
