package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;

import java.util.List;

public interface AnalysisService {

    void save(Analysis analysis);

    Analysis getAnalysisById(Long id);

    List<Analysis> getAnalysisByShowIds(Long[] id);

    Analysis getAnalysisByShowName(Show show);

    Show getBestAnalysis(Long[] showIds);

    Show getBestShowByAnalysis(List<Analysis> analysisList);

}
