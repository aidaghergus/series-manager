package ro.ase.eu.sm.seriesmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.*;
import ro.ase.eu.sm.seriesmanager.model.dto.CommentDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ReviewDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.UserGeneralDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.repository.ShowRepository;
import ro.ase.eu.sm.seriesmanager.service.CommentService;
import ro.ase.eu.sm.seriesmanager.service.ReviewService;
import ro.ase.eu.sm.seriesmanager.service.ShowService;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.Constants;
import ro.ase.eu.sm.seriesmanager.utils.SuccesMessage;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class UserController {

    private static final Logger LOGGER = new Logger(UserController.class);

    @Autowired
    RepositoryMapper repositoryMapper;

    @Autowired
    UserService userService;

    @Autowired
    ShowService showService;

    @Autowired
    CommentService commentService;

    @Autowired
    ReviewService reviewService;

    @GetMapping("/users")
    public List<UserGeneralDTO> findByFilter(@RequestHeader(value= Constants.AUTHORIZATION) String token, @RequestParam String filter) {
        LOGGER.info(filter, InfoMessage.REQUEST_RECEIVE + RequestType.FIND_USERS);
        userService.authenticateWithToken(token);
        List<UserGeneralDTO> userGeneralDTOList = userService.findByFilter(filter).stream().map(UserMapper::mapToUserGeneralDTO).filter(user -> !user.getRole().equals(Constants.ROLE_ADMIN)).collect(Collectors.toList());
        LOGGER.info(userGeneralDTOList, SuccesMessage.SEND_USERS);
        return userGeneralDTOList;
    }

    @PostMapping("/addshow/{showId}")
    public void addSeenShow(@RequestHeader(value = Constants.AUTHORIZATION) String token, @PathVariable Long showId){
        Show showDTO=showService.findShowById(showId);
        LOGGER.info(showDTO, InfoMessage.REQUEST_RECEIVE + RequestType.ADD_SHOW);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        userService.saveSeenShow(u,showDTO);
        LOGGER.info(SuccesMessage.SUCCES_ADD_SHOW);
    }

    @GetMapping("/seenshows")
    public List<ShowDTO> getSeenShows(@RequestHeader(value = Constants.AUTHORIZATION) String token){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_SEEN_SHOWS);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        return userService.findAllSeenShows(u).stream().map(ShowMapper::mapToDTO).collect(Collectors.toList());
    }





}
