package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;

public interface ReviewService {

    void save(Review review);

    List<Review> findByUser(User user);

    List<Review> findByShow(Long show);

    Review findReviewForShowByUser(User user, Show show);


}
