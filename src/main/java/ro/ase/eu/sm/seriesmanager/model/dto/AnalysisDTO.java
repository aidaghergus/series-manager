package ro.ase.eu.sm.seriesmanager.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class AnalysisDTO {
    private Long show_id;

    private String show_name;

    private Double textLengthAvg;

    private Double htmlLengthAvg;

    private Double outLinksAvg;

    private String sentimentTones;

    private Integer sentimentScore;

    private Double searchesLevel;

    private Double imdbRating;

    private Integer imdbVotes;

    private Double externalRating;



}
