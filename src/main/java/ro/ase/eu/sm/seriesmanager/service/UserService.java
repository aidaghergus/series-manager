package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    User findById(Long id);

    void signUp(User user);

    Token authenticateWithCredentials(User user);

    Long authenticateWithToken(String token);

    List<User> findAll();

    List<User> findByFilter(String filter);

    Set<Show> findAllSeenShows(User user);

    void saveSeenShow(User user, Show show);

}
