package ro.ase.eu.sm.seriesmanager.errorhandling.exception.general;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
}
