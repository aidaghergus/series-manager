package ro.ase.eu.sm.seriesmanager.security;

import org.springframework.stereotype.Component;
import ro.ase.eu.sm.seriesmanager.errorhandling.error.ErrorMessage;
import ro.ase.eu.sm.seriesmanager.errorhandling.exception.general.InternalServerErrorException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class PasswordCrypter {

    public String getCryptedPassword(String password) throws RuntimeException {

        try {
            String generatedPassword = null;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
            return generatedPassword;
        }
        catch(NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(ErrorMessage.INTERNAL_SERVER_ERROR);
        }
    }
}
