package ro.ase.eu.sm.seriesmanager.mapper;


import ro.ase.eu.sm.seriesmanager.model.dto.UserDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.UserGeneralDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.UserRevComDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.model.validator.UserDTOValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class UserMapper {

    private UserMapper(){}

    public static User mapToEntity(UserDTO userDto, RepositoryMapper mapper) {
        UserDTOValidator.validate(userDto);
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setGender(userDto.getGender());
        user.setPhone(userDto.getPhone());
        user.setRole(mapper.getRoleRepository().findByRoleName(userDto.getRole()));
        return user;
    }

    public static User mapToUserCredentials(UserDTO userDto, RepositoryMapper mapper) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        return user;
    }

    public static UserDTO mapToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setEmail(user.getEmail());
        userDTO.setGender(user.getGender());
        userDTO.setPhone(user.getPhone());
        userDTO.setRole(user.getRole().getRoleName());
        return userDTO;
    }

    public static UserGeneralDTO mapToUserGeneralDTO(User user) {
        UserGeneralDTO userGeneralDTO = new UserGeneralDTO();
        userGeneralDTO.setId(user.getId());
        userGeneralDTO.setFirstName(user.getFirstName());
        userGeneralDTO.setLastName(user.getLastName());
        userGeneralDTO.setUsername(user.getUsername());
        userGeneralDTO.setEmail(user.getEmail());
        userGeneralDTO.setGender(user.getGender());
        userGeneralDTO.setPhone(user.getPhone());
        userGeneralDTO.setRole(user.getRole().getRoleName());
        return userGeneralDTO;
    }

    public static UserRevComDTO mapToUserRevComDTO(User user){
        return new UserRevComDTO(user.getId(),user.getUsername());
    }

    public static User mapToEntity(UserRevComDTO userRevComDTO, RepositoryMapper repositoryMapper){
        return repositoryMapper.getUserRepository().findOne(userRevComDTO.getId());
    }

}
