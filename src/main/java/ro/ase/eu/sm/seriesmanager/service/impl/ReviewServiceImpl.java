package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.repository.ReviewRepository;
import ro.ase.eu.sm.seriesmanager.service.ReviewService;

import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public void save(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public List<Review> findByUser(User user) {
        return reviewRepository.findAllByUserId(user.getId());
    }

    @Override
    public List<Review> findByShow(Long show) {
        return reviewRepository.findAllByShowId(show);
    }

    @Override
    public Review findReviewForShowByUser(User user, Show show) {
        List<Review> reviews=reviewRepository.findAllByUserAndShow(user,show);
        if(reviews==null)
            return null;
        return reviews.get(0);
    }
}
