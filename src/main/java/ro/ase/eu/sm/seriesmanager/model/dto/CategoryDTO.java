package ro.ase.eu.sm.seriesmanager.model.dto;


import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class CategoryDTO {

    private String name;
}
