package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.Episode;
import ro.ase.eu.sm.seriesmanager.repository.EpisodeRepository;
import ro.ase.eu.sm.seriesmanager.service.EpisodeService;

import java.util.List;

@Service
public class EpisodeServiceImpl implements EpisodeService {

    @Autowired
    EpisodeRepository episodeRepository;

    @Override
    public Episode findEpisodeById(Long episodeId) {
        return episodeRepository.findOne(episodeId);
    }

    @Override
    public List<Episode> getEpisodesByShowAndSeason(Long showId, String season) {
        return episodeRepository.findAllByShowIdAndAndSeason(showId,season);
    }
}
