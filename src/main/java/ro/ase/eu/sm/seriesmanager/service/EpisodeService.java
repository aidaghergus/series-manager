package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Episode;

import java.util.List;

public interface EpisodeService {

    Episode findEpisodeById(Long episodeId);

    List<Episode> getEpisodesByShowAndSeason(Long showId, String season);
}
