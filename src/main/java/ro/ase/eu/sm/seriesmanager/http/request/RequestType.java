package ro.ase.eu.sm.seriesmanager.http.request;

public enum RequestType {

    SIGN_UP ("SIGN UP"),
    LOG_IN("LOG IN"),
    FIND_USERS("FIND_USERS"),
    GET_SHOW_COMMENTS("GET COMMENTS BY SHOW"),
    GET_CATEGORIES("GET CATEGORIES"),
    GET_SHOW_REVIEWS("GET REVIEWS BY SHOW"),
    GET_ROLES("GET ROLES"),
    GET_ALL_SHOWS("GET ALL SHOWS"),
    GET_SIX_SHOWS("GET SIX SHOWS"),
    ADD_SHOW_DATABASE("ADD SHOW DATABASE"), GET_SEARCHED_SHOWS("GET SEARCHED SHOWS"),
    GET_SHOWS_BY_CATEGORIES("GET_SHOWS_BY_CATEGORIES"), GET_TWITTER_NEWS("GET TWITTER NEWS"),
    CRAWLER_OUTPUT("CRAWLER OUTPUT"),
    ADD_SHOW("ADD SHOW"),
    ADD_REVIEW("ADD REVIEW"),
    GET_ANALYSIS("GET ANALYSIS"),
    GET_SEEN_SHOWS("GET SEEN SHOWS"),
    GET_USER_COMMENTS("GET USER COMMENTS"),
    POST_COMMENT("POST COMMENT"),
    GET_ALL_USER_REVIEWS("GET ALL USER REVIEWS"),
    GET_ALL_USER_REVIEWS_BY_SHOW("GET ALL USER REVIEWS BY SHOW");

    private final String label;

    private RequestType(String label) {
        this.label=label;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
