package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.errorhandling.error.ErrorMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.CommentDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.CommentUserDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Comment;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.utils.DateFormatter;

import java.text.ParseException;

public class CommentMapper {
    private static final Logger LOGGER = new Logger(CommentMapper.class);

    private CommentMapper(){}

    public static Comment mapToEntity(CommentDTO commentDTO, RepositoryMapper mapper){
        Comment comment=new Comment();
        //comment.setId(commentDTO.getId());
        comment.setContent(commentDTO.getContent());
        /*try {
            comment.setReviewDate(DateFormatter.formatter.parse(commentDTO.getCommentDate()));
        } catch (ParseException e) {
            LOGGER.error(ErrorMessage.INVALID_DATE_FORMAT);
            e.printStackTrace();
        }*/

        comment.setUser(UserMapper.mapToEntity(commentDTO.getUser(),mapper));
        comment.setShow(ShowMapper.mapToEntity(commentDTO.getShow(),mapper));

        return comment;
    }

    public static CommentDTO mapToDTO(Comment comment){
        return CommentDTO.builder()
                .id(comment.getId())
                .content(comment.getContent())
                .commentDate(DateFormatter.formatter.format(comment.getReviewDate()))
                .user(UserMapper.mapToUserRevComDTO(comment.getUser()))
                .show(ShowMapper.mapToShowRevComDTO(comment.getShow()))
                .build();
    }

    public static CommentUserDTO mapToCommentUserDTO(Comment comment){
        return new CommentUserDTO(comment.getId(),comment.getReviewDate().toString(),comment.getContent(),ShowMapper.mapToShowRevComDTO(comment.getShow()));
    }

    public static Comment mapToEntity(CommentUserDTO commentUserDTO, RepositoryMapper mapper){
        return mapper.getCommentRepository().findOne(commentUserDTO.getId());
    }

}
