package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.errorhandling.error.ErrorMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.ReviewDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ReviewUserDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.utils.DateFormatter;

import java.text.ParseException;

public class ReviewMapper {
    private static final Logger LOGGER = new Logger(ReviewMapper.class);

    private ReviewMapper(){}

    public static Review mapToEntity(ReviewDTO reviewDTO, RepositoryMapper mapper){
        Review review=new Review();
        review.setStars(reviewDTO.getStars());
        review.setComment(reviewDTO.getComment());
        /*try {
            review.setReviewDate(DateFormatter.formatter.parse(reviewDTO.getReviewDate()));
        } catch (ParseException e) {
            LOGGER.error(ErrorMessage.INVALID_DATE_FORMAT);
            e.printStackTrace();
        }*/
        review.setUser(UserMapper.mapToEntity(reviewDTO.getUser(),mapper));

        review.setShow(ShowMapper.mapToEntity(reviewDTO.getShow(),mapper));

        return review;
    }

    public static ReviewDTO mapToDTO(Review review){
        return ReviewDTO.builder()
                .id(review.getId())
                .stars(review.getStars())
                .reviewDate(DateFormatter.formatter.format(review.getReviewDate()))
                .comment(review.getComment())
                .user(UserMapper.mapToUserRevComDTO(review.getUser()))
                .show(ShowMapper.mapToShowRevComDTO(review.getShow()))
                .build();
    }

    public static ReviewUserDTO mapToReviewUserDTO(Review review){
        return new ReviewUserDTO(review.getId(),review.getStars(),review.getReviewDate().toString(),review.getComment(),ShowMapper.mapToShowRevComDTO((review.getShow())));
    }

    public static Review mapToEntity(ReviewUserDTO reviewUserDTO, RepositoryMapper mapper){
        return mapper.getReviewRepository().findOne(reviewUserDTO.getId());
    }

}
