package ro.ase.eu.sm.seriesmanager.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity(name="roles")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EnableAutoConfiguration
public class Role {

    @Id
    @Column(name="role")
    private String roleName;

    @Column(name="privilege",nullable=false)
    @NotNull
    private String privilege;

}
