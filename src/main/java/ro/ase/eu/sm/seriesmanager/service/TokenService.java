package ro.ase.eu.sm.seriesmanager.service;


import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;

public interface TokenService {

    List<Token> findByUser(User user);

    Token findByToken(String token);

    User getUser(String token);

    void save(Token token);

    void delete(Token token);

}
