package ro.ase.eu.sm.seriesmanager.errorhandling.exception.general;

public class BadRequestException extends RuntimeException {

    public BadRequestException(final String error) {
        super(error);
    }

}
