package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.analysis.CrawlerOutput;
import ro.ase.eu.sm.seriesmanager.analysis.GoogleAPIController;
import ro.ase.eu.sm.seriesmanager.analysis.WatsonAPI;
import ro.ase.eu.sm.seriesmanager.controller.TwitterController;
import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.repository.AnalysisRepository;
import ro.ase.eu.sm.seriesmanager.repository.ShowRepository;
import ro.ase.eu.sm.seriesmanager.service.AnalysisService;

import javax.persistence.EntityNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AnalysisImpl implements AnalysisService {

    @Autowired
    AnalysisRepository analysisRepository;

    @Autowired
    ShowRepository showRepository;

    @Override
    public void save(Analysis analysis) {
        analysisRepository.save(analysis);
    }

    @Override
    public Analysis getAnalysisById(Long id) {

        return analysisRepository.findOne(id);
    }

    @Override
    public List<Analysis> getAnalysisByShowIds(Long[] ids) {
        List<Analysis> result=new ArrayList<>();
        Analysis analysis;
        for(Long id:ids){
            analysis=getAnalysisById(id);
            if(analysis==null)
                analysis=doAnalysis(id);
            result.add(analysis);
        }
        return result;
    }

    @Override
    public Analysis getAnalysisByShowName(Show show) {
        return analysisRepository.getOne(show.getId());
    }

    @Override
    public Show getBestAnalysis(Long[] showIds) {

        for(int i=0;i<showIds.length;i++){
            doAnalysis(showIds[i]);
        }

        Analysis firstAnalysis;
        try{
            firstAnalysis=analysisRepository.getOne(showIds[0]);}
        catch(EntityNotFoundException e){
            this.doAnalysis(showIds[0]);
            firstAnalysis=analysisRepository.getOne(showIds[0]);

        }

        int maxFinalScore=firstAnalysis.getSentimentScore();
        Show bestShow=firstAnalysis.getShow();
        for(int i=1;i<showIds.length;i++){
            Analysis analysis;
            try{
                analysis=analysisRepository.getOne(showIds[i]);
            }
            catch (EntityNotFoundException e){
                this.doAnalysis(showIds[i]);
                analysis=analysisRepository.getOne(showIds[i]);
            }

            int finalScore=analysis.getSentimentScore();
            if(finalScore>maxFinalScore) {
                maxFinalScore = finalScore;
                bestShow=analysis.getShow();
            }
        }
        return bestShow;
    }

    @Override
    public Show getBestShowByAnalysis(List<Analysis> analysisList) {
        Show bestShow=null;

        //text,html,link,sentiment,search,imdbr,imdbv,rating
        int[] besties=new int[] {0,0,0,0,0,0,0,0};

        for(int i=1;i<analysisList.size();i++){
            if(analysisList.get(i).getTextLengthAvg()>analysisList.get(besties[0]).getTextLengthAvg())
                besties[0]=i;
            if(analysisList.get(i).getHtmlLengthAvg()>analysisList.get(besties[1]).getHtmlLengthAvg())
                besties[1]=i;
            if(analysisList.get(i).getOutLinksAvg()>analysisList.get(besties[2]).getOutLinksAvg())
                besties[2]=i;
            if(analysisList.get(i).getSentimentScore()>analysisList.get(besties[3]).getSentimentScore())
                besties[3]=i;
            if(analysisList.get(i).getSearchesLevel()>analysisList.get(besties[4]).getSearchesLevel())
                besties[4]=i;
            if(analysisList.get(i).getImdbRating()>analysisList.get(besties[5]).getImdbRating())
                besties[5]=i;
            if(analysisList.get(i).getImdbVotes()>analysisList.get(besties[6]).getImdbVotes())
                besties[6]=i;
            if(analysisList.get(i).getExternalRating()>analysisList.get(besties[7]).getExternalRating())
                besties[7]=i;
        }

        int[] freq=new int[analysisList.size()];
        for(int i=0;i<besties.length;i++)
            freq[besties[i]]++;

        int max=freq[0];
        int maxPoz=0;
        for(int i=1;i<freq.length;i++)
            if(freq[i]>max) {
                max = freq[i];
                maxPoz=i;
            }

        bestShow=analysisList.get(maxPoz).getShow();

        return  bestShow;
    }

    public Analysis doAnalysis(Long showId){

        //get show
        Show show=showRepository.findOne(showId);

        //crawler analisys
        CrawlerOutput crawlerOutput=CrawlerOutput.createAverage(GoogleAPIController.crawl(show.getName().replaceAll(" ",",")));

        //twitter + watson analysis
        SearchResults results=TwitterController.twitter.searchOperations().search("#"+show.getName().replaceAll(" ",""),5);

        String result;
        int sentimentScore=0;
        Set<String> tones=new HashSet<>();
        for(int i=0;i<results.getTweets().size();i++) {
            result = WatsonAPI.getTones(results.getTweets().get(i).getText());
            sentimentScore+=WatsonAPI.getScore(result);
            for(String a:result.split(" "))
                tones.add(a);
        }
        StringBuilder sentimentTones=new StringBuilder("");
        for(String tone: tones)
            sentimentTones.append(tone).append(" ");

        Integer totalSearches=showRepository.searches();

        DecimalFormat decim = new DecimalFormat("0.00");

        //create analysis object
        Analysis analysis=Analysis.builder().show(show)
                .show_id(show.getId())
               .textLengthAvg(Double.parseDouble(decim.format(crawlerOutput.getTextLength())))
               .htmlLengthAvg(Double.parseDouble(decim.format(crawlerOutput.getHtmlLength())))
               .outLinksAvg(Double.parseDouble(decim.format(crawlerOutput.getOutLinks())))
               .sentimentTones(sentimentTones.toString())
               .sentimentScore(sentimentScore)
               .searchesLevel(Double.parseDouble(decim.format(Double.parseDouble(show.getSearches().toString())/totalSearches)))
               .imdbRating(show.getImdbRating())
               .imdbVotes(show.getImdbVotes())
               .externalRating(show.getExternalRating())
               .build();

        //save analysis to database
        this.save(analysis);

        return analysis;
    }
}
