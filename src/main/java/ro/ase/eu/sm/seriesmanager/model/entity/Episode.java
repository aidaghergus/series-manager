package ro.ase.eu.sm.seriesmanager.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity(name="episodes")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Episode {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="episode_id")
    private Long id;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="season", nullable=false)
    private String season;

    @Column(name="number", nullable = false)
    private String number;

    @ManyToOne
    @JoinColumn(name="show_id")
    @NotNull
    private Show show;
}
