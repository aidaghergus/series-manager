package ro.ase.eu.sm.seriesmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.CategoryMapper;
import ro.ase.eu.sm.seriesmanager.mapper.ShowMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.CategoryDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.service.CategoryService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CategoryController {

    private static final Logger LOGGER = new Logger(CategoryController.class);

    @Autowired
    CategoryService categoryService;


    @GetMapping("/categories")
    public List<CategoryDTO> getAllCategories(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_CATEGORIES);
        return categoryService.findAllCategories().stream().map(CategoryMapper::mapToDTO).collect(Collectors.toList());
    }


    @GetMapping("/categories/search/{categoryId}")
    public List<ShowDTO> getSeriesByCategories(@PathVariable String[] categoryId){

        Set<Show> allShows=new HashSet<>();
        for(String id:categoryId){
            allShows.addAll(categoryService.findAllShowsForCategory(id));
        }

        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_SHOWS_BY_CATEGORIES);

        return allShows.stream().map(ShowMapper::mapToDTO).collect(Collectors.toList());
    }
}
