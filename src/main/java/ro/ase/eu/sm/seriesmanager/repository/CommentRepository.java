package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ase.eu.sm.seriesmanager.model.entity.Comment;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByUserId(Long id);

    List<Comment> findAllByShowId(Long id);
}
