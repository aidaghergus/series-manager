package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Category;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;

import java.util.List;

public interface CategoryService {

    List<Category> findAllCategories();

    List<Show> findAllShowsForCategory(String id);

    Category findCategoryById(String id);

    void addShowCategory(Show show, Category category);

    void save(Category category);
}
