package ro.ase.eu.sm.seriesmanager.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class EpisodeDTO {

    private Long id;

    private String name;

    private String season;

    private String number;

    private Long showId;
}
