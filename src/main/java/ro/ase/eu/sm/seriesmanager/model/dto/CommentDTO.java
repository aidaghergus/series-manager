package ro.ase.eu.sm.seriesmanager.model.dto;


import lombok.*;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class CommentDTO {

    private Long id;

    private String commentDate;

    private String content;

    private UserRevComDTO user;

    private ShowRevComDTO show;

}
