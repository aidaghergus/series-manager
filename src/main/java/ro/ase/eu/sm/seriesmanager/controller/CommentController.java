package ro.ase.eu.sm.seriesmanager.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.*;
import ro.ase.eu.sm.seriesmanager.model.dto.*;
import ro.ase.eu.sm.seriesmanager.model.entity.Comment;
import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.CommentService;
import ro.ase.eu.sm.seriesmanager.service.RoleService;
import ro.ase.eu.sm.seriesmanager.service.ShowService;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.Constants;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CommentController {

    private static final Logger LOGGER = new Logger(CommentController.class);

    @Autowired
    CommentService commentService;

    @Autowired
    UserService userService;

    @Autowired
    ShowService showService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @GetMapping("/{showId}/comments")
    public List<CommentDTO> getAllShowComments(@PathVariable Long showId) {
        LOGGER.info(InfoMessage.REQUEST_RECEIVE + RequestType.GET_SHOW_COMMENTS);
        return commentService.findByShow(showId).stream().map(CommentMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/user/comments")
    public List<CommentDTO> getShowComments(@RequestHeader(value = Constants.AUTHORIZATION) String token){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_USER_COMMENTS);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        return commentService.findByUser(u).stream().map(CommentMapper::mapToDTO).collect(Collectors.toList());
    }

    @PostMapping("/{showId}/addcomment")
    public void postShowComments(@RequestHeader(value = Constants.AUTHORIZATION) String token,@PathVariable Long showId, @RequestBody CommentDTO commentDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.POST_COMMENT);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        Show show=showService.findShowById(showId);
        u.setShows(null);
        UserRevComDTO userDTO=UserMapper.mapToUserRevComDTO(u);
        ShowRevComDTO showDTO=ShowMapper.mapToShowRevComDTO(show);

        CommentDTO newComment=commentDTO;
        newComment.setUser(userDTO);
        newComment.setShow(showDTO);
        Comment comment=CommentMapper.mapToEntity(newComment,repositoryMapper);
        commentService.save(comment);
    }



    //TODO-comments&reviewuser, loghez tweets, signup page

}
