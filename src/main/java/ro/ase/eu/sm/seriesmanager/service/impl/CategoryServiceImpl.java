package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.Category;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.repository.CategoryRepository;
import ro.ase.eu.sm.seriesmanager.repository.ShowRepository;
import ro.ase.eu.sm.seriesmanager.service.CategoryService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ShowRepository showRepository;

    @Override
    public List<Category> findAllCategories() {
        return categoryRepository.findAll();
    }



    @Override
    public List<Show> findAllShowsForCategory(String id) {
        return categoryRepository.findOne(id).getShowsList();
    }

    @Override
    public Category findCategoryById(String id) {
        return categoryRepository.findOne(id);
    }

    @Override
    public void addShowCategory(Show show, Category category) {
        /*List<Show> list=category.getShowsList();
        if(list==null)
            list=new ArrayList<>();
        list.add(show);
        category.setShowsList(list);
        categoryRepository.save(category);*/

        List<Category> categories=show.getCategorieList();
        if(categories==null)
            categories=new ArrayList<>();
        categories.add(category);
        show.setCategorieList(categories);
        showRepository.save(show);

    }

    @Override
    public void save(Category category) {
        categoryRepository.save(category);
    }
}
