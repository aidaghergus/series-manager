package ro.ase.eu.sm.seriesmanager.model.dto;


import lombok.*;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ReviewDTO {

    private Long id;

    private Integer stars;

    private String reviewDate;

    private String comment;

    private UserRevComDTO user;

    private ShowRevComDTO show;
}
