package ro.ase.eu.sm.seriesmanager.analysis;

import com.ibm.watson.developer_cloud.tone_analyzer.v3.ToneAnalyzer;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneAnalysis;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneOptions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.social.twitter.api.SearchResults;
import ro.ase.eu.sm.seriesmanager.controller.TwitterController;

import java.util.*;

public class WatsonAPI {

    public static String VERSION_DATE="2017-09-21";
    public static String watsonUser="4e364477-7eca-4b41-a46f-5bcdbe6dcda6";
    public static String watsonPass="hmy7pG2KexdT";
    public static String endpoint="https://gateway.watsonplatform.net/tone-analyzer/api";

    //anger, fear, joy, and sadness
    public static final HashMap<String,Integer> toneScores=new HashMap<String,Integer>();
    static{
        toneScores.put("anger",-2);
        toneScores.put("fear",-1);
        toneScores.put("sadness",0);
        toneScores.put("joy",3);
    }

    public static String getTones(String text){
        ToneAnalyzer toneAnalyzer = new ToneAnalyzer(VERSION_DATE);

        toneAnalyzer.setUsernameAndPassword(watsonUser, watsonPass);
        toneAnalyzer.setEndPoint(endpoint);

        ToneOptions toneOptions = new ToneOptions.Builder()
                .addTone(ToneOptions.Tone.EMOTION)
                .html(text).build();

        ToneAnalysis tone = toneAnalyzer.tone(toneOptions).execute();
        JSONObject myResponse = new JSONObject(tone.toString());
        JSONArray toneCategories=myResponse.getJSONObject("document_tone").getJSONArray("tones");

        StringBuilder toneIds=new StringBuilder("");
        for(int i=0;i<toneCategories.length();i++){
            JSONObject ton=new JSONObject(toneCategories.get(i).toString());
            toneIds.append(ton.getString("tone_id")).append(" ");
        }
        return toneIds.toString();
    }

    public static Integer getScore(String tones){
        String[] toneKeys=tones.split(" ");
        Integer finalScore=0;
        for (String tonKey:toneKeys) {
                if(toneScores.get(tonKey)!=null)
                    finalScore+=toneScores.get(tonKey);
        }
        return  finalScore;
    }

    public static void main(String[] args){
        SearchResults results= TwitterController.twitter.searchOperations().search("#breakingbad",5);

        String result;
        int sentimentScore=0;
        Set<String> tones=new HashSet<>();
        for(int i=0;i<results.getTweets().size();i++) {
            result = WatsonAPI.getTones(results.getTweets().get(i).getText());
            sentimentScore+=WatsonAPI.getScore(result);
            for(String a:result.split(" "))
                tones.add(a);
        }
        StringBuilder sentimentTones=new StringBuilder("");
        for(String tone: tones)
            sentimentTones.append(tone).append(" ");

        System.out.println("Sentiment tones: " +sentimentTones);
        System.out.println("Sentiment score: "+sentimentScore);

    }
}
