package ro.ase.eu.sm.seriesmanager.model.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class TokenDTO implements Serializable {

    private String token;
    private UserGeneralDTO user;
}
