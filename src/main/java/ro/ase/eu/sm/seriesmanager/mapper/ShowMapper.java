package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowRevComDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;

import java.util.stream.Collectors;

public class ShowMapper {
    private static final Logger LOGGER = new Logger(ShowMapper.class);

    private ShowMapper(){}

    public static Show mapToEntity(ShowDTO showDTO, RepositoryMapper mapper){
        Show show=new Show();
        show.setName(showDTO.getName());
        show.setDescription(showDTO.getDescription());
        show.setYear(showDTO.getYear());
        show.setId(showDTO.getId());
        show.setImage(showDTO.getImage());
        if(showDTO.getCategoryList()!= null)
            show.setCategorieList(showDTO.getCategoryList().stream().map(category -> CategoryMapper.mapToEntity(category, mapper)).collect(Collectors.toList()));
        if(showDTO.getEpisodeList()!= null)
            show.setEpisodeList(showDTO.getEpisodeList().stream().map(episodeDTO -> EpisodeMapper.mapToEntity(episodeDTO,mapper)).collect(Collectors.toList()));
        if(showDTO.getReviewList()!= null)
            show.setReviews(showDTO.getReviewList().stream().map(reviewDTO -> ReviewMapper.mapToEntity(reviewDTO,mapper)).collect(Collectors.toList()));
        if(showDTO.getCommentList()!= null)
            show.setComments(showDTO.getCommentList().stream().map(commentDTO -> CommentMapper.mapToEntity(commentDTO,mapper)).collect(Collectors.toList()));
        show.setActors(showDTO.getActors());
        show.setAwards(showDTO.getAwards());
        show.setImdbRating(showDTO.getImdbRating());
        show.setImdbVotes(showDTO.getImdbVotes());
        show.setExternalRating(showDTO.getExternalRating());
        show.setSearches(showDTO.getSearches());


        return show;
    }

    public static ShowDTO mapToDTO(Show show){
        return ShowDTO.builder()
                .id(show.getId())
                .name(show.getName())
                .description(show.getDescription())
                .year(show.getYear())
                .image(show.getImage())
                .categoryList(show.getCategorieList()!=null?show.getCategorieList().stream().map(CategoryMapper::mapToDTO).collect(Collectors.toList()):null)
                .episodeList(show.getEpisodeList()!=null?show.getEpisodeList().stream().map(EpisodeMapper::mapToDTO).collect(Collectors.toList()):null)
                .commentList(show.getComments()!=null?show.getComments().stream().map(CommentMapper::mapToDTO).collect(Collectors.toList()):null)
                .reviewList(show.getReviews()!=null?show.getReviews().stream().map(ReviewMapper::mapToDTO).collect(Collectors.toList()):null)
                .actors(show.getActors())
                .awards(show.getAwards())
                .imdbRating(show.getImdbRating())
                .imdbVotes(show.getImdbVotes())
                .externalRating(show.getExternalRating())
                .searches(show.getSearches())
                .build();
    }

    public static Show mapToEntity(ShowRevComDTO showRevComDTO, RepositoryMapper repositoryMapper){
        return repositoryMapper.getShowRepository().findOne(showRevComDTO.getId());
    }

    public static ShowRevComDTO mapToShowRevComDTO(Show show){
        return new ShowRevComDTO(show.getId(),show.getName(),show.getImage());
    }

}
