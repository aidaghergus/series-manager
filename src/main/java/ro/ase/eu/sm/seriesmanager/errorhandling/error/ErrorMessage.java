package ro.ase.eu.sm.seriesmanager.errorhandling.error;


import ro.ase.eu.sm.seriesmanager.utils.DateFormatter;

public interface ErrorMessage {

	String NOT_AUTHENTICATED = "Authentication failed";
	String SESSION_EXPIRED = "Session expired";
	String NOT_ROLE_FOUND = "Invalid role";
	String INVALID_USERNAME = "Selected username exists";
	String NULL_ATRIBUTES = "These fields are empty: ";
	String REQUIRED_USERNAME_PASSWORD= "Username and password are required";
	String INVALID_CREDENTIALS= "Invalid username or password";
	String USER_IS_REQUIRED= "User id is required";
	String INVALID_TOKEN = "Invalid token ";
	String INTERNAL_SERVER_ERROR = "Internal server error";
	String ALL_FIELDS_ARE_REQUIRED = "All fields are reequired";
	String SERVICE_UNAVAILABLE = "Service unavailable";

    String INVALID_DATE_FORMAT = "Invalid date format. Correct format is " + DateFormatter.formatter.toPattern();


}
