package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.errorhandling.error.ErrorMessage;
import ro.ase.eu.sm.seriesmanager.errorhandling.exception.user.*;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.repository.RoleRepository;
import ro.ase.eu.sm.seriesmanager.repository.UserRepository;
import ro.ase.eu.sm.seriesmanager.security.Authenticator;
import ro.ase.eu.sm.seriesmanager.security.PasswordCrypter;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.SuccesMessage;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = new Logger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    Authenticator authenticator;

    @Autowired
    PasswordCrypter passwordCrypter;

    public User findById(Long id) {
        return userRepository.findOne(id);
    }

    public void signUp(User user) throws RuntimeException {
        if(user.getRole()==null) {
            LOGGER.error(ErrorMessage.NOT_ROLE_FOUND);
            throw new InvalidRoleException(ErrorMessage.NOT_ROLE_FOUND);
        }
        User existingUser = userRepository.findByUsername(user.getUsername());
        if(existingUser!=null) {
            LOGGER.error(ErrorMessage.INVALID_USERNAME);
            throw new InvalidUsernameException(ErrorMessage.INVALID_USERNAME);
        }
        user.setPassword(passwordCrypter.getCryptedPassword(user.getPassword()));
        userRepository.save(user);
        LOGGER.info(user, SuccesMessage.SUCCES_SIGN_UP);
    }

    public Token authenticateWithCredentials(User user) throws RuntimeException {
        if(!user.hasCredentials()) {
            LOGGER.error(ErrorMessage.REQUIRED_USERNAME_PASSWORD);
            throw new CredentialsRequiredException(ErrorMessage.REQUIRED_USERNAME_PASSWORD);
        }
        User existingUser = userRepository.findByUsernameAndPassword(user.getUsername(),passwordCrypter.getCryptedPassword(user.getPassword()));
        if(existingUser==null) {
            LOGGER.error(ErrorMessage.INVALID_CREDENTIALS);
            throw new InvalidCredentialsException(ErrorMessage.INVALID_CREDENTIALS);
        }
        Token token = authenticator.obtainToken(existingUser);
        return token;
    }


    public Long authenticateWithToken(String token) throws NotAuthorizedException {
        return authenticator.authenticateWithToken(token);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findByFilter(String filter) {
        return userRepository.findByFilter(filter);
    }

    @Override
    public Set<Show> findAllSeenShows(User user) {
        return user.getShows();
    }

    @Override
    public void saveSeenShow(User user, Show show) {
        Set<Show> newShowList=user.getShows();
        newShowList.add(show);
        user.setShows(newShowList);
        userRepository.save(user);
    }
}
