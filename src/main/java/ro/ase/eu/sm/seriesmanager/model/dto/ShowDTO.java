package ro.ase.eu.sm.seriesmanager.model.dto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ShowDTO implements Comparable {


    private Long id;

    private String name;

    private String description;

    private Integer year;

    private String image;

    private List<EpisodeDTO> episodeList;

    private List<CategoryDTO> categoryList;

    private List<CommentDTO> commentList;

    private List<ReviewDTO> reviewList;

    private Double imdbRating;

    private Integer imdbVotes;

    private Double externalRating;

    private String actors;

    private String awards;

    private Integer searches;


    @Override
    public int compareTo(Object o) {
        ShowDTO show = (ShowDTO) o;
        return this.getId().compareTo(show.getId());
    }
}
