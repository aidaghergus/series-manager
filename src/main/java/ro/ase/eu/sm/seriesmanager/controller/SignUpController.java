package ro.ase.eu.sm.seriesmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.RepositoryMapper;
import ro.ase.eu.sm.seriesmanager.mapper.UserMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.UserDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.UserService;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class SignUpController {

    private static final Logger LOGGER = new Logger(SignUpController.class);

    @Autowired
    RepositoryMapper mapper;

    @Autowired
    UserService userService;

    @PostMapping("/signup")
    public void signUp(@RequestBody UserDTO userDTO) {
        LOGGER.info(userDTO, InfoMessage.REQUEST_RECEIVE + RequestType.SIGN_UP);
        User user = UserMapper.mapToEntity(userDTO,mapper);
        userService.signUp(user);
    }

}
