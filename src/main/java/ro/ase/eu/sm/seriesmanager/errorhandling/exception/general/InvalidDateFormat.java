package ro.ase.eu.sm.seriesmanager.errorhandling.exception.general;

public class InvalidDateFormat extends RuntimeException {

    public InvalidDateFormat(final String error) {
        super(error);
    }
}
