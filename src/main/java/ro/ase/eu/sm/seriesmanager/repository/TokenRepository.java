package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    List<Token> findByUser(User user);
}
