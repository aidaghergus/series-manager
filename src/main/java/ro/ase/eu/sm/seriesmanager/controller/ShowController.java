package ro.ase.eu.sm.seriesmanager.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.RepositoryMapper;
import ro.ase.eu.sm.seriesmanager.mapper.ShowMapper;
import ro.ase.eu.sm.seriesmanager.mapper.UserMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;
import ro.ase.eu.sm.seriesmanager.model.entity.Category;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.AnalysisService;
import ro.ase.eu.sm.seriesmanager.service.CategoryService;
import ro.ase.eu.sm.seriesmanager.service.ShowService;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ShowController {

    private static final Logger LOGGER = new Logger(ShowController.class);

    private static Integer totalSearches=0;

    @Autowired
    private ShowService showService;

    @Autowired
    private UserService userService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @Autowired
    private AnalysisService analysisService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/shows")
    public List<ShowDTO> getAllShows(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_SHOWS);
        return showService.findAll().stream().map(ShowMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/topshows")
    public List<ShowDTO> getTopShows(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+RequestType.GET_SIX_SHOWS);
        //return showService.findAll().subList(0,6).stream().map(ShowMapper::mapToDTO).collect(Collectors.toList());
        return showService.findTopShows().stream().map(ShowMapper::mapToDTO).collect(Collectors.toList());
    }




    @GetMapping("/shows/search/{text}")
    public ShowDTO getSearchedShows(@PathVariable String text){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_SEARCHED_SHOWS);


        Show newShow=showService.findShowInDatabase(text);

        if(newShow!=null) {
            newShow.setSearches(newShow.getSearches()+1);
            showService.save(newShow);
            return ShowMapper.mapToDTO(showService.findShowInDatabase(text));
        }


        text=text.replaceAll(" ","+");

        String SERVER = "http://www.omdbapi.com/?t=";
        String APIKEY="&apikey=4338e08a&";
        String request=SERVER+text+APIKEY;

        URL obj = null;
        Analysis newAnalysis=null;
        try {
            obj = new URL(request);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");


            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + request);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print in String Error
            System.out.println(response.toString());
            //Read JSON response and print/*
            JSONObject myResponse = new JSONObject(response.toString());
            /*String showId;
            try {
                showId = myResponse.getString("imdbID").substring(3);
            }
            catch(Exception e){
                LOGGER.info("Show not found");
                return null;
            }*/
            String showId=myResponse.getString("imdbID").substring(3);
            String showName=myResponse.getString("Title");
            String showDescription=myResponse.getString("Plot");
            String showYear=myResponse.getString("Year").replaceAll("[^\\d.]", "").substring(0,4);
            String showImage=myResponse.getString("Poster");
            String imdbRaiting=myResponse.getString("imdbRating");
            String imdbVotes=myResponse.getString("imdbVotes").replaceAll(",","");
            String actors=myResponse.getString("Actors");
            String awards=myResponse.getString("Awards");
            String externalRating=myResponse.getJSONArray("Ratings").getJSONObject(0).getString("Value").split("/")[0];
            String[] genre=myResponse.getString("Genre").split(" ");

            newShow=showService.findShowById(Long.parseLong(showId));
            if(newShow==null)
                newShow=new Show(Long.parseLong(showId),showName,showDescription,Integer.parseInt(showYear),showImage,Double.parseDouble(imdbRaiting),Integer.parseInt(imdbVotes),Double.parseDouble(externalRating),actors,awards,0,null,null,null,null);

            newShow.setSearches(1);
            showService.save(newShow);
            System.out.println("Show saved: "+ newShow);
            //Analysis analysis=new Analysis(showId,crawlerOutput.getTextLength(),crawlerOutput.getHtmlLength(),crawlerOutput.getOutLinks(),0,0.0,0.0,0,0.0,show);

            //analysisService.save();

            String categories=myResponse.getString("Genre");
            System.out.println(categories);

            for(String s:categories.replaceAll(",","").split(" ")){
                Category category=categoryService.findCategoryById(s);
                if(category==null) {
                    category=new Category(s,null);
                    categoryService.save(category);
                }
                categoryService.addShowCategory(newShow,category);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ShowMapper.mapToDTO(newShow);

    }
}
