package ro.ase.eu.sm.seriesmanager.model.dto;

import lombok.*;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ReviewUserDTO implements Serializable {

    private Long id;

    private Integer stars;

    private String reviewDate;

    private String comment;

    private ShowRevComDTO show;

}
