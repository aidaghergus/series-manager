package ro.ase.eu.sm.seriesmanager.utils;

public interface Constants {

    String AUTHORIZATION = "Authorization";
    String ROLE_ADMIN = "admin";
}
