package ro.ase.eu.sm.seriesmanager.logger;

public interface InfoMessage {
    String REQUEST_RECEIVE = "Request received for ";
    String TOKEN_OBTAINED = "Token obtained: ";
    String SUCCESFULLY_AUTH_TOKEN = "Succesfully authentication with token";
}
