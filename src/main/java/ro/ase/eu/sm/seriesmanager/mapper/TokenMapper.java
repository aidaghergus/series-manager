package ro.ase.eu.sm.seriesmanager.mapper;


import ro.ase.eu.sm.seriesmanager.model.dto.TokenDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;

public class TokenMapper {

    private TokenMapper() {

    }

    public static Token mapToEntity(TokenDTO tokenDto, RepositoryMapper mapper) {
        return mapper.getTokenRepository().findByToken(tokenDto.getToken());
    }

    public static TokenDTO mapToDTO(Token token) {
        TokenDTO tokenDTO = TokenDTO.builder().token(token.getToken()).user(UserMapper.mapToUserGeneralDTO(token.getUser())).build();
        return tokenDTO;
    }

}
