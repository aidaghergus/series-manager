package ro.ase.eu.sm.seriesmanager.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.RepositoryMapper;
import ro.ase.eu.sm.seriesmanager.mapper.TokenMapper;
import ro.ase.eu.sm.seriesmanager.mapper.UserMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.TokenDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.UserDTO;
import ro.ase.eu.sm.seriesmanager.service.UserService;

import java.net.Authenticator;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class LogInController {

    private static final Logger LOGGER = new Logger(LogInController.class);

    @Autowired
    UserService userService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @PostMapping("/login")
    public TokenDTO logIn(@RequestBody UserDTO userDTO) {
        LOGGER.info(userDTO, InfoMessage.REQUEST_RECEIVE + RequestType.LOG_IN);
        return TokenMapper.mapToDTO(userService.authenticateWithCredentials(UserMapper.mapToUserCredentials(userDTO,repositoryMapper)));
    }
}
