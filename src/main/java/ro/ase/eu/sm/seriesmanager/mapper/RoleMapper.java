package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.model.dto.RoleDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Role;

public class RoleMapper {

    private RoleMapper() {

    }

    public static Role mapToEntity(RoleDTO roleDTO, RepositoryMapper mapper) {
        return mapper.getRoleRepository().findByRoleName(roleDTO.getRoleName());
    }

    public static RoleDTO mapToDTO(Role role) {
        return RoleDTO.builder().roleName(role.getRoleName()).build();
    }
}
