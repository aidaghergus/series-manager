package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class InvalidCredentialsException  extends RuntimeException {

    public InvalidCredentialsException(final String error) {
        super(error);
    }
}
