package ro.ase.eu.sm.seriesmanager.errorhandling.exception.general;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String error) {
        super(error);
    }
}
