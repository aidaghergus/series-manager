package ro.ase.eu.sm.seriesmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.Constants;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class TwitterController {

    private static final Logger LOGGER = new Logger(TwitterController.class);

    private static final String consumerKey = "hU33Jt083IcuHP5dZGFQuFyWj"; // The application's consumer key
    private static final String consumerSecret = "TGaIRFls5nk9EFDB9SPPkEk5uoO6olELxYDm8WJmJ8XLaaTcQX"; // The application's consumer secret
    private static final String accessToken = "922871623564382210-aq6lbI1W0tzA7yfs5Aur9csGarDkezf"; // The access token granted after OAuth authorization
    private static final String accessTokenSecret = "cnqrzdDWTFFj6evrzc3yeGiaYAjvpZZ3y7QvoPUQxJVxr"; // The access token secret granted after OAuth authorization
    public static final Twitter twitter = new TwitterTemplate(consumerKey, consumerSecret, accessToken, accessTokenSecret);

    @Autowired
    UserService userService;

    //@RequestHeader(value= Constants.AUTHORIZATION) String token

    @GetMapping("/twitter")
    public List<Tweet> getAllTweets(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_TWITTER_NEWS);
       //Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(new Long(2));
        int nr=userService.findAllSeenShows(u).size();
        LOGGER.info("NR"+nr);

        if(nr==0)
            return  null;

        List<Tweet> finalTweets=new ArrayList<>();

        SearchResults results=null;
        for(Show show:userService.findAllSeenShows(u)){
            results=TwitterController.twitter.searchOperations().search("#"+show.getName().replaceAll(" ",""),5/nr);
            finalTweets.addAll(results.getTweets());
        }

        return finalTweets;
    }


    @GetMapping("/twitterdoi")
    public SearchResults getAllTweetss(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_TWITTER_NEWS);
        return twitter.searchOperations().search("#homeland",5);
    }



}
