package ro.ase.eu.sm.seriesmanager.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages="ro.ase.eu.sm.seriesmanager")
@EnableJpaRepositories("ro.ase.eu.sm.seriesmanager.repository")
@EntityScan("ro.ase.eu.sm.seriesmanager.model")
public class SeriesManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeriesManagerApplication.class, args);
	}
}
