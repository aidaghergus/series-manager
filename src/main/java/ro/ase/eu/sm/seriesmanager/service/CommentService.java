package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Comment;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;

public interface CommentService {

    void save(Comment comment);

    List<Comment> findByUser(User user);

    List<Comment> findByShow(Long showId);


}
