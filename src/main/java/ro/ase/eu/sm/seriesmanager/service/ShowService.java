package ro.ase.eu.sm.seriesmanager.service;



import ro.ase.eu.sm.seriesmanager.model.entity.*;

import java.util.List;


public interface ShowService {

    List<Show> findAll();

    Show findShowById(Long showId);

    List<Category> findShowCategories(Long showId);

    Double getShowStars(Long showId);

    List<Episode> findShowEpisodes(Long showId);

    List<Review> findShowReviews(Long showId);

    List<Comment> findShowComments(Long showId);

    List<Episode> findEpisodesBySeason(Long showId, String season);

    Show findShowInDatabase(String filter);

    void save(Show show);

    List<Show> findTopShows();



}
