package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    @Query("SELECT u FROM users u WHERE UPPER(u.firstName) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.lastName) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.username) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.email) LIKE CONCAT('%',UPPER(:filter),'%')")
    List<User> findByFilter(@Param("filter") String filter);

}
