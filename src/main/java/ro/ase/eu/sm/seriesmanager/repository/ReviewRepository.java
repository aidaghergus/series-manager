package ro.ase.eu.sm.seriesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ase.eu.sm.seriesmanager.model.entity.Comment;
import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findAllByShowId(Long id);

    List<Review> findAllByUserId(Long id);

    List<Review> findAllByUserAndShow(User user, Show show);
}
