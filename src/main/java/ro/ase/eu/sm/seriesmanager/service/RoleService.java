package ro.ase.eu.sm.seriesmanager.service;

import ro.ase.eu.sm.seriesmanager.model.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();
}
