package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class NotAuthorizedException extends RuntimeException {

    public NotAuthorizedException(final String error) {
        super(error);
    }
}
