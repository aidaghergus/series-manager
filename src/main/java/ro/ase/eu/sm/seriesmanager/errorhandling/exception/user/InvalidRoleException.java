package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException(final String error) {
        super(error);
    }
}