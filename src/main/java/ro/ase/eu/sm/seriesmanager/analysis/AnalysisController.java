package ro.ase.eu.sm.seriesmanager.analysis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.AnalysisMapper;
import ro.ase.eu.sm.seriesmanager.mapper.ShowMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.AnalysisDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;
import ro.ase.eu.sm.seriesmanager.service.AnalysisService;
import ro.ase.eu.sm.seriesmanager.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AnalysisController {

    public static final Logger LOGGER=new Logger(AnalysisController.class);

    @Autowired
    AnalysisService analysisService;

    List<Analysis> lastAnalysis;

    @GetMapping("/analysis/best")
    public ShowDTO getBestShow(){
        //return ShowMapper.mapToDTO(analysisService.getBestAnalysis(shows));
        return ShowMapper.mapToDTO(analysisService.getBestShowByAnalysis(lastAnalysis));

    }

    @GetMapping("/analysis/{ids}")
    public List<AnalysisDTO> getShowAnalysis(@PathVariable Long[] ids){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ANALYSIS);
        ids= Utils.removeDuplicates(ids);
        lastAnalysis=analysisService.getAnalysisByShowIds(ids);
        return lastAnalysis.stream().map(AnalysisMapper::mapToDTO).collect(Collectors.toList());

    }


}
