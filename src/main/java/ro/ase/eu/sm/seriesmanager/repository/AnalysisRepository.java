package ro.ase.eu.sm.seriesmanager.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ro.ase.eu.sm.seriesmanager.model.entity.Analysis;

public interface AnalysisRepository extends JpaRepository<Analysis, Long> {
}
