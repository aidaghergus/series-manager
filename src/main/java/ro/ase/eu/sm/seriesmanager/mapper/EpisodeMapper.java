package ro.ase.eu.sm.seriesmanager.mapper;

import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.dto.EpisodeDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Episode;

public class EpisodeMapper {

    private static final Logger LOGGER = new Logger(CommentMapper.class);

    public EpisodeMapper(){}

    public static Episode mapToEntity(EpisodeDTO episodeDTO, RepositoryMapper repositoryMapper){
        Episode episode=new Episode(episodeDTO.getId(),episodeDTO.getName(),episodeDTO.getSeason(),episodeDTO.getNumber(),null);
        episode.setShow(repositoryMapper.getShowRepository().getOne(episodeDTO.getShowId()));
        return episode;

    }

    public static EpisodeDTO mapToDTO(Episode episode){
        return EpisodeDTO.builder()
                .id(episode.getId())
                .name(episode.getName())
                .season(episode.getSeason())
                .number(episode.getNumber())
                .showId(episode.getShow().getId())
                .build();
    }

}
