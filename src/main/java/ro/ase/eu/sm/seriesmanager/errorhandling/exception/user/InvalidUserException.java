package ro.ase.eu.sm.seriesmanager.errorhandling.exception.user;

public class InvalidUserException extends RuntimeException {

    public InvalidUserException(String message) {
        super(message);
    }
}
