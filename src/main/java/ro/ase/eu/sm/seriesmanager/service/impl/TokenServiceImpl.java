package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.model.entity.Token;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.repository.TokenRepository;
import ro.ase.eu.sm.seriesmanager.service.TokenService;

import java.util.List;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    public List<Token> findByUser(User user) {
        return tokenRepository.findByUser(user);
    }

    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public User getUser(String token) {
        Token existingToken = findByToken(token);
        return existingToken.getUser();
    }

    public void save(Token token) {
        tokenRepository.save(token);
    }

    public void delete(Token token) {
        tokenRepository.delete(token);
    }
}
