package ro.ase.eu.sm.seriesmanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.model.entity.Role;
import ro.ase.eu.sm.seriesmanager.repository.RoleRepository;
import ro.ase.eu.sm.seriesmanager.service.RoleService;
import ro.ase.eu.sm.seriesmanager.utils.SuccesMessage;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = new Logger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleJpaRepository;

    public List<Role> findAll() {
        LOGGER.info(SuccesMessage.ROLES_SENDED);
        return roleJpaRepository.findAll();
    }
}
