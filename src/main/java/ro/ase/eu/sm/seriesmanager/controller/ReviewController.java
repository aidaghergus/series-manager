package ro.ase.eu.sm.seriesmanager.controller;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;
import ro.ase.eu.sm.seriesmanager.mapper.RepositoryMapper;
import ro.ase.eu.sm.seriesmanager.mapper.ReviewMapper;
import ro.ase.eu.sm.seriesmanager.mapper.ShowMapper;
import ro.ase.eu.sm.seriesmanager.mapper.UserMapper;
import ro.ase.eu.sm.seriesmanager.model.dto.ReviewDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.ShowRevComDTO;
import ro.ase.eu.sm.seriesmanager.model.dto.UserRevComDTO;
import ro.ase.eu.sm.seriesmanager.model.entity.Review;
import ro.ase.eu.sm.seriesmanager.model.entity.Show;
import ro.ase.eu.sm.seriesmanager.model.entity.User;
import ro.ase.eu.sm.seriesmanager.service.ReviewService;
import ro.ase.eu.sm.seriesmanager.service.ShowService;
import ro.ase.eu.sm.seriesmanager.service.UserService;
import ro.ase.eu.sm.seriesmanager.utils.Constants;
import ro.ase.eu.sm.seriesmanager.utils.SuccesMessage;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ReviewController {

    private static final Logger LOGGER = new Logger(ReviewController.class);

    @Autowired
    ReviewService reviewService;

    @Autowired
    UserService userService;

    @Autowired
    ShowService showService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @GetMapping("/{showId}/reviews")
    public List<ReviewDTO> getAllShowReviews(@PathVariable Long showId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_SHOW_REVIEWS);
        return reviewService.findByShow(showId).stream().map(ReviewMapper::mapToDTO).collect(Collectors.toList());
    }

    @PostMapping("{showId}/addreview")
    public void addReview(@RequestHeader(value = Constants.AUTHORIZATION) String token,@PathVariable Long showId, @RequestBody ReviewDTO reviewDTO){
        LOGGER.info(reviewDTO, InfoMessage.REQUEST_RECEIVE + RequestType.ADD_REVIEW);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        Show show=showService.findShowById(showId);
        u.setShows(null);
        UserRevComDTO userDTO=UserMapper.mapToUserRevComDTO(u);
        ShowRevComDTO showDTO=ShowMapper.mapToShowRevComDTO(show);

        ReviewDTO newComment=reviewDTO;
        newComment.setUser(userDTO);
        newComment.setShow(showDTO);
        Review comment=ReviewMapper.mapToEntity(newComment,repositoryMapper);
        reviewService.save(comment);
        LOGGER.info(SuccesMessage.SUCCES_ADD_REVIEW);
    }

    @GetMapping("/user/reviews")
    public List<ReviewDTO> getUserReviews(@RequestHeader(value = Constants.AUTHORIZATION) String token){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_USER_REVIEWS);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        return reviewService.findByUser(u).stream().map(ReviewMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/user/reviews/{showId}")
    public ReviewDTO getUserShowReview(@RequestHeader(value = Constants.AUTHORIZATION) String token, @PathVariable Long showId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_USER_REVIEWS_BY_SHOW);
        Long userId=userService.authenticateWithToken(token);
        User u=userService.findById(userId);
        Show show=showService.findShowById(showId);
        Review review=reviewService.findReviewForShowByUser(u,show);
        if(review==null)
            return null;
        return ReviewMapper.mapToDTO(review);
    }

}
