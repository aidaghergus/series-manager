package ro.ase.eu.sm.seriesmanager.analysis;


import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.springframework.web.bind.annotation.*;
import ro.ase.eu.sm.seriesmanager.http.request.RequestType;
import ro.ase.eu.sm.seriesmanager.logger.InfoMessage;
import ro.ase.eu.sm.seriesmanager.logger.Logger;

import javax.json.Json;
import javax.json.stream.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class GoogleAPIController {

    private static final Logger LOGGER = new Logger(GoogleAPIController.class);

    private final static String apiKey = "AIzaSyC6MgMiGLaFAtFT2Ks4kAc4YS-dv8nwQ0E";
    private final static String customSearchEngineKey ="007748084996925876473:mxpo3kim4im";
    private final static String searchURL = "https://www.googleapis.com/customsearch/v1?";

    @GetMapping("/crawler/{search}")
    public CrawlerOutput averageCrawler(@PathVariable String search){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.CRAWLER_OUTPUT);
        return CrawlerOutput.createAverage(GoogleAPIController.crawl(search));
    }

    public static List<CrawlerOutput> crawl(String search) {
        String crawlStorageFolder = "src/main/data/crawl";
        int numberOfCrawlers = 1;

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setMaxDepthOfCrawling(1);
        config.setMaxPagesToFetch(3);

        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = null;
        try {
            controller = new CrawlController(config, pageFetcher, robotstxtServer);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> links=new ArrayList<String>();
        String result = "";
        result = read(search, 1, 3);
        JsonParser parser = Json.createParser(new StringReader(result));
        while (parser.hasNext()) {
            JsonParser.Event event = parser.next();
            if (event == JsonParser.Event.KEY_NAME) {
                if (parser.getString().equals("htmlTitle")) {
                    JsonParser.Event value = parser.next();
                    if (value == JsonParser.Event.VALUE_STRING)
                        System.out.println("Title (HTML): " + parser.getString());
                }
                if (parser.getString().equals("link")) {
                    JsonParser.Event value = parser.next();
                    if (value == JsonParser.Event.VALUE_STRING) {
                        System.out.println("Link: " + parser.getString());
                        links.add(parser.getString());
                    }
                }
            }
        }
        for(String s: links){
            controller.addSeed(s);
        }
        controller.start(MyCrawler.class, numberOfCrawlers);
        return MyCrawler.output;
    }

    private static String read(String qSearch, int start, int numOfResults) {
        try {

            String toSearch = searchURL + "key=" + apiKey + "&cx="
            + customSearchEngineKey + "&q=";

            toSearch += qSearch;

            toSearch += "&alt=json";

            toSearch += "&start=" + start;

            toSearch += "&num=" + numOfResults;

            URL url = new URL(toSearch);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            StringBuffer buffer = new StringBuffer();
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void main(String[] args){
        CrawlerOutput crawlerOutput=CrawlerOutput.createAverage(GoogleAPIController.crawl("Breaking+bad"));
        System.out.println(crawlerOutput.getHtmlLength()+" "+crawlerOutput.getOutLinks()+" "+crawlerOutput.getTextLength());
    }

}