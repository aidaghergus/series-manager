package ro.ase.eu.sm.seriesmanager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeriesManagerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
